<?php


    require_once '../persistencia/ControladorPersistencia.php';
    $refPersistencia = ControladorPersistencia::obtenerCP();
    try {
        $refPersistencia->iniciarTransaccion();
        
        $formulario = $_GET['formulario'];
        $accion = $_GET['accion'];
        $controlador = 'Controlador' . $formulario;
        require_once '../controladoresEspecificos/' . $controlador . '.php';
        $datosFormulario = $_POST;
        $refControlador = new $controlador($datosFormulario);
        $resultado = $refControlador->$accion($datosFormulario);
        echo json_encode($resultado);
        $refPersistencia->confirmarTransaccion();
    } catch (Exception $ex) {
        echo $ex->getMessage();
        $refPersistencia->rollBackTransaccion();
    } finally {
        $refPersistencia->cerrarConexion();
    }
  